const jwt = require("jsonwebtoken");
const axios = require('axios')
const userController = {};
const { User } = require("../models/index");
const { encryptPassword } = require("../utils");
const {twelveData, twelveDataDetail, twelveDataDetailRealTime, twelveDataDetailHistorical} = require("../services/twelveData")

userController.register = async (req, res) => {
  const { userName, password, passwordConfirm } = req.body;
  try {

    // Verificamos que el usuario no este registrado en la db
    const userRegistered = await User.findOne({ userName });
    if (userRegistered) {
      return res.status(400).json({
        message: "El email ya se encuentra registrado, utilice uno nuevo.",
        userRegister: true,
      });
    }

    // Verificamos que las claves coincidan
    if (password !== passwordConfirm) return res.status(403).json({ message: "Las contraseñas deben ser iguales." });

    // Creamos usuario y hasheamos password
    const newUser = new User({
      userName,
      password,
    });

    newUser.password = await newUser.encryptPassword(password);

    // guardamos usuario en la db
    const savedUser = await newUser.save();

    res.status(200).json({ message: "Usuario creado satisfactoriamente", savedUser })
  } catch (error) {

    res.status(400).json({ message: "Error", error });
  }
};

userController.login = async (req, res, next) => {
  const { userName, password } = req.body;

  try {
    // Validamos que el usuario ingresado exista.
    const user = await User.findOne({ userName });
    if (!user) return res.status(400).json({ message: "Usuario no encontrado" })

    // Validamos que la contrasena sea correcta
    const validPassword = await user.matchPassword(password);
    if (!validPassword) return res.status(400).json({ message: "Password Incorrecta" });

    // Creamos token de login y le indicamos que dura 24 horas
    const token = jwt.sign(
      { _id: user._id },
      process.env.TOKEN_SECRET
    );
    res.header("auth-token", token).status(200).json({ token, user });
  } catch (error) {
    console.log('error', error)
    res.status(400).json({ message: "Error", error });

  }
};


userController.logout = async (req, res) => {
  const id = req.user;
  try {
    if (!id)return res.status(400).json({ message: "No se puede llevar a cabo esta accion, ya que no hay ningun usuario logueado" })

    req.user = undefined;
    res.status(200).json({ message: "Usuario deslogueado satisfactoriamente" })
  }
  catch (error) {
    console.log('error', error)
    res.status(400).json({ message: "Error", error });
  }

};

userController.info = async (req, res) => {
  const id = req.user;

  try {
    if (!id)return res.status(400).json({ message: "No se puede llevar a cabo esta accion, ya que no hay ningun usuario logueado" })
    const request = await twelveData()
    res.status(200).json({ message: "Info correctamente", request })
  }
  catch (error) {
    console.log('error', error)
    res.status(400).json({ message: "Error", error });
  }

};

userController.addAction = async (req, res) => {
  const myAction = req.body;
  const id = req.user
  try {
    // Verificamos que el usuario exista
    const userRegistered = await User.findOne({ id });
    if (!userRegistered) {
      return res.status(400).json({
        message: "El usuario no existe.",
        userRegister: false,
      });
    }

    await userRegistered.userActions.push(req.body)
    const userUpdated = await userRegistered.save()
    res.status(200).json({ message: "Accion agregada correctamente", userUpdated })
  } catch (error) {
    console.log('error', error)
    res.status(400).json({ message: "Error", error });
  }
};

userController.getActions = async (req, res) => {
  const id = req.user
  let myActions = []
  try {
    // Verificamos que el usuario exista
    const userRegistered = await User.findOne({ id });
    if (!userRegistered) {
      return res.status(400).json({
        message: "El usuario no existe.",
        userRegister: false,
      });
    }

    myActions = userRegistered.userActions
    res.status(200).json({ message: "Acciones mostradas correctamente", myActions })
  } catch (error) {
    console.log('error', error)
    res.status(400).json({ message: "Error", error });
  }
};

userController.deleteAction = async (req, res) => {
  const id = req.user
  const symbol = req.params.symbol
  try {
    // Verificamos que el usuario exista
    const userRegistered = await User.findOne({ id });
    if (!userRegistered) {
      return res.status(400).json({
        message: "El usuario no existe.",
        userRegister: false,
      });
    }

    const userActionFilter = userRegistered.userActions.filter((userAction)=> userAction.symbol !== symbol)
    userRegistered.userActions = userActionFilter
    await userRegistered.save()

    res.status(200).json({ message: "Acciones mostradas correctamente", userRegistered })
  } catch (error) {
    console.log('error', error)
    res.status(400).json({ message: "Error", error });
  }
};

userController.getActionDetail = async (req, res) => {
  const id = req.user;
  const symbol = req.params.symbol
  try {
    if (!id)return res.status(400).json({ message: "No se puede llevar a cabo esta accion, ya que no hay ningun usuario logueado" })
    const request = await twelveDataDetail(symbol)
    res.status(200).json({ message: "Info correctamente", request })
  }
  catch (error) {
    console.log('error', error)
    res.status(400).json({ message: "Error", error });
  }
};

userController.getActionDetailRealTime = async (req, res) => {
  const id = req.user;
  const symbol = req.params.symbol
  const interval = req.params.interval
  try {
    if (!id)return res.status(400).json({ message: "No se puede llevar a cabo esta accion, ya que no hay ningun usuario logueado" })
    const request = await twelveDataDetailRealTime(symbol,interval)
    res.status(200).json({ message: "Info correctamente", request })
  }
  catch (error) {
    console.log('error', error)
    res.status(400).json({ message: "Error", error });
  }
};

userController.getActionDetailHistorical = async (req, res) => {
  const id = req.user;
  const symbol = req.params.symbol
  const startDate = req.params.startdate
  const endDate = req.params.enddate


  try {
    if (!id)return res.status(400).json({ message: "No se puede llevar a cabo esta accion, ya que no hay ningun usuario logueado" })
    const request = await twelveDataDetailHistorical(symbol,startDate,endDate)
    res.status(200).json({ message: "Info correctamente", request })
  }
  catch (error) {
    console.log('error', error)
    res.status(400).json({ message: "Error", error });
  }
};






module.exports = userController;