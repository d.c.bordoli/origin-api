const router = require("express").Router();
const userController = require("../controllers/userController");
const verifyJWT = require("../middlewares/tokens/jwtController");

//* Registrar a un usuario
router.post('/register', userController.register)

//* Loguear a un usuario
router.post('/login', userController.login)

//* Desloguear a un usuario
router.get("/logout", verifyJWT, userController.logout);

//* API con info
router.get("/info", verifyJWT, userController.info)

//* Agregar Accion
router.post("/action/add", verifyJWT, userController.addAction)

//* Mostrar Acciones de un usuario
router.get("/action/myactions", verifyJWT, userController.getActions)

//* Borrar una accion de un usuario
router.post("/action/delete/:symbol", verifyJWT, userController.deleteAction)

//* Mostrar detalles de una accion
router.get("/action/detail/:symbol", verifyJWT, userController.getActionDetail)

//* Mostrar accion en real time
router.get("/action/detail/realtime/:symbol/:interval", verifyJWT, userController.getActionDetailRealTime)

//* Mostrar historico de accion
router.get("/action/detail/historical/:symbol/:startdate/:enddate", verifyJWT, userController.getActionDetailHistorical)





module.exports = router;