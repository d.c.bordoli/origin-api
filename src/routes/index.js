const router = require("express").Router();

const Api = require ('./api')
const User = require ('./user')

router.use("/api", Api);
router.use("/user", User)



module.exports = router;
