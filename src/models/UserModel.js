const { model, Schema } = require("../config/db/index");
const bcrypt = require("bcryptjs");

const UserSchema = new Schema({
  userName: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  userActions: []
});

UserSchema.methods.encryptPassword = async (password) => {
  const salt = await bcrypt.genSalt(10);
  return await bcrypt.hash(password, salt);
};

UserSchema.methods.matchPassword = async function (password) {
  const resu = await bcrypt.compare(password, this.password);
  return resu;
};

module.exports = model("users", UserSchema);