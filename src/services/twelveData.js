const axios = require("axios");

const twelveData = async () => {
  try {
    const request = await axios.get('https://api.twelvedata.com/stocks?source=docs&exchange=NYSE')
    return request.data;
  } catch (err) {
    console.error(err);
  }
};

const twelveDataDetail = async (symbol) => {
  try {
    const request = await axios.get(`https://api.twelvedata.com/stocks?symbol=${symbol}&source=docs`)
    return request.data;
  } catch (err) {
    console.error(err);
  }
};

const twelveDataDetailRealTime = async (symbol,interval) => {
  try {
    const request = await axios.get(`https://api.twelvedata.com/time_series?symbol=${symbol}&interval=${interval}&apikey=f7cde9861dde4994808e8f7b280da400`)
    return request.data;
  } catch (err) {
    console.error(err);
  }
};

const twelveDataDetailHistorical = async (symbol,startDate, endDate) => {
  try {
    const request = await axios.get(`https://api.twelvedata.com/time_series?&start_date=${startDate}&end_date=${endDate}&symbol=${symbol}&interval=1day&apikey=f7cde9861dde4994808e8f7b280da400`)
    return request.data;
  } catch (err) {
    console.error(err);
  }
};




module.exports = {
    twelveData,
    twelveDataDetail,
    twelveDataDetailRealTime,
    twelveDataDetailHistorical
};